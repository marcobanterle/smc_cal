\documentclass{article}
\include{preamble}
%\usepackage{caption}

\title{Bayesian analysis of calcium traces}
\begin{document}
\maketitle
\section{Exponential model of Calcium activity}
We model the fluorescence trace $\lbrace Y_t\rbrace_{t=1}^T$ as the sum of a Brownian motion $B_t$ with transition probability 
\begin{align}
f_B(b_t|b_{t-1})=\mathcal{N}(b_t|b_{t-1},\sigma_B)
\end{align}
and a second Markov process $C_t$ quantifying calcium level whose transition probability is given in terms of a hidden binary activity state $s_t\in \lbrace0,1\rbrace$ 
\begin{align}
f^{s_t}_C(c_t|c_{t-1})=
\left\lbrace\begin{array}{l r}
					\gamma_C\exp\left\lbrace -\gamma_C(c_t-c_{t-1}e^{-\lambda\delta t})\right\rbrace\mathbb{I}(c_t>c_{t-1}e^{-\lambda\delta t}) & \mathrm{if\;} s_t=1 \\
					\delta\left(c_t-c_{t-1}e^{-\lambda\delta t}\right) & \mathrm{if\;} s_t=0 
					\end{array}\right.
\end{align}
where $s_t$ is a Bernoulli process with
\begin{align}
f_S(s_t)=(q)^{s_t}(1-q)^{1-s_t}.
\end{align}
The initial probability of the latent variables is
\begin{align}
\mu(b_1,c_1,s_1)=\delta_{s_1,0}\cdot \delta(c_1-c_0) \cdot \delta(b_1-b_0) 
\end{align}
The emission probability, i.e. the probability of observing $y_t$ given $\lbrace c_t,b_t\rbrace$ at any time $1\le t\le T$ 
\begin{align}
g(y_t|c_t,b_t)=\mathcal{N}(y_t|c_t+b_t, \sigma_Y)
\end{align}
in the following we will denote $x_t\equiv\lbrace b_t,c_t,s_t\rbrace$.
We can express the probability $P(x_t|x_{t-1},y_t)$, for any $t>1$, in terms of the transition functions as
\begin{align}
P(x_t|x_{t-1},y_t)=P(s_t|x_{t-1},y_t)\cdot P(c_t|x_{t-1},s_t,y_t)\cdot P(b_t|x_{t-1},s_t,c_t,y_t)\label{eq:post_fact}
\end{align}
The first factor corresponds to the probability of the activity state at time $t$ given the previous hidden state $x_{t-1}$ and the current fluorescence level, which we can write as
\begin{align}
P(s_t|x_{t-1},y_t)&= \frac{1}{Z_S} \int db_t dc_t P(c_t,y_t,b_t,s_t| x_{t-1})=\nonumber\\
&= \frac{1}{Z_S} \int db_t dc_t f_S(s_t) f_C^{s_t}(c_t|c_{t-1})f_B(b_t|b_{t-1})g(y_t|c_t,b_t)
\end{align}
where the normalization constant $Z_S=P(y_t|x_{t-1})$ is the marginal likelihood of the current observation conditional to the latent variables at time $t-1$.
If $s_t=0$ we have
\begin{align}
P(s_t=0|x_{t-1},y_t)&= \frac{(1-q)}{Z_S} \int db_t dc_t \delta\left(c_t-c_{t-1}e^{-\lambda\delta t}\right)\frac{1}{\sqrt{2\pi \sigma_B^2}\sqrt{2\pi \sigma_Y^2}} \exp\left\lbrace -\frac{(b_t-b_{t-1})^2}{2\sigma_B^2}-\frac{(y_t-b_t-c_t)^2}{2\sigma_Y^2}\right\rbrace=\nonumber\\
&= \frac{(1-q)}{Z_S} \int db_t \frac{1}{\sqrt{2\pi \sigma_B^2}\sqrt{2\pi \sigma_Y^2}} \exp\left\lbrace -\frac{(b_t-b_{t-1})^2}{2\sigma_B^2}-\frac{(y_t-b_t-c_{t-1}e^{-\lambda\delta t})^2}{2\sigma_Y^2}\right\rbrace=\nonumber\\
&=\frac{(1-q)}{Z_S\sqrt{2\pi(\sigma_B^2+\sigma_Y^2)}} \exp\left\lbrace -\frac{1}{2}\frac{(y_t-c_{t-1}e^{-\lambda\delta t}-b_{t-1})^2}{\sigma_B^2+\sigma_Y^2}\right\rbrace\label{eq:s0}
\end{align}
whereas if $s_t=1$ we have
\begin{align}
P(s_t=1|x_{t-1},y_t)&= \frac{q}{Z_S} \int db_t dc_t\frac{\gamma_C \exp\left\lbrace -\frac{(b_t-b_{t-1})^2}{2\sigma_B^2}-\frac{(y_t-b_t-c_t)^2}{2\sigma_Y^2}-\gamma_C(c_t-c_{t-1}e^{-\lambda\delta t})\right\rbrace}{\sqrt{2\pi \sigma_B^2}\sqrt{2\pi \sigma_Y^2}}=\nonumber\\
&=\frac{q\gamma_C }{Z_S\sqrt{2\pi(\sigma_B^2+\sigma_Y^2)}} \int^{+\infty}_{c_{t-1}e^{-\lambda\delta t}} dc_t \exp\left\lbrace -\frac{1}{2}\frac{(c_t+b_{t-1}-y_t)^2}{\sigma_B^2+\sigma_Y^2}-\gamma_C(c_t-c_{t-1}e^{-\lambda\delta t})\right\rbrace=\nonumber\\
&= \frac{q\gamma_C\exp\left\lbrace \gamma_C c_{t-1}e^{-\lambda\delta t}\right\rbrace }{Z_S\sqrt{2\pi(\sigma_B^2+\sigma_Y^2)}} \int^{+\infty}_{c_{t-1}e^{-\lambda\delta t}} dc_t \exp\left\lbrace -\frac{1}{2}\frac{(c_t+b_{t-1}-y_t)^2}{\sigma_B^2+\sigma_Y^2}-\gamma_C c_t\right\rbrace
\end{align}
we can now use the identity
\begin{align}
\int_u^{+\infty} e^{-\frac{(x-x_0)^2}{2\sigma^2}-\gamma x} = e^{-\gamma x_0 +\frac{1}{2}\gamma^2\sigma^2}\sigma\sqrt{\frac{\pi}{2}} \mathrm{Erfc}\left(\frac{u-x_0}{\sigma\sqrt{2}}+\frac{\gamma\sigma}{\sqrt{2}}\right)
\end{align}
to obtain
\begin{align}
P(s_t=1|x_{t-1},y_t)&= \frac{q\gamma_C\exp\left\lbrace -\gamma_C (y_t-b_{t-1}-c_{t-1}e^{-\lambda\delta t})+\frac{1}{2}\gamma_C^2(\sigma_B^2+\sigma_Y^2)\right\rbrace }{Z_S\sqrt{2\pi(\sigma_B^2+\sigma_Y^2)}}\cdot\nonumber\\
&\cdot \sqrt{\frac{\pi (\sigma_B^2+\sigma_Y^2)}{2}} \mathrm{Erfc}\left(\frac{c_{t-1}e^{-\lambda\delta t}-y_t+b_{t-1}}{\sqrt{2(\sigma_B^2+\sigma_Y^2)}}+\gamma_C \sqrt{\frac{\sigma_B^2+\sigma_Y^2}{2}}\right)=\nonumber\\
&= \frac{q\gamma_C}{2Z_S} \exp\left\lbrace -\gamma_C (y_t-b_{t-1}-c_{t-1}e^{-\lambda\delta t})+\frac{1}{2}\gamma_C^2(\sigma_B^2+\sigma_Y^2)\right\rbrace\cdot\nonumber\\
&\cdot\mathrm{Erfc}\left(\frac{c_{t-1}e^{-\lambda\delta t}-y_t+b_{t-1}}{\sqrt{2(\sigma_B^2+\sigma_Y^2)}}+\gamma_C \sqrt{\frac{\sigma_B^2+\sigma_Y^2}{2}}\right)\label{eq:s1}
\end{align}
From which we can calculate the normalization constant $Z_S$
\begin{align}
Z_S=&\frac{(1-q)}{\sqrt{2\pi\sigma^2}} e^{ -\frac{z_t^2}{2\sigma^2}}+\frac{q\gamma_C}{2} e^{ \gamma_C z_t+\frac{1}{2}\gamma_C^2\sigma^2}\mathrm{Erfc}\left(\frac{z_t}{\sqrt{2}\sigma}+\gamma_C \frac{\sigma}{\sqrt2}\right)=\nonumber\\
=& \frac{(1-q)}{\sqrt{2\pi\sigma^2}} e^{ -\frac{z_t^2}{2\sigma^2}} \left[ 1+ 
\frac{q}{(1-q)}\frac{\sqrt{\pi} \gamma_C\sigma}{\sqrt2} e^{\frac{1}{2}\left(\frac{z_t}{\sigma}+\gamma_C\sigma \right)^2} \mathrm{Erfc}\left(\frac{z_t/\sigma+\gamma_C \sigma}{\sqrt2}\right)\right]\label{eq:ZS}
\end{align}
where we defined
\begin{align}
z_t&= c_{t-1}e^{-\lambda\delta t}-y_t+b_{t-1}\nonumber\\
\sigma^2&=\sigma_B^2+\sigma_Y^2
\end{align}
Let us now compute the second factor in Eq.~(\ref{eq:post_fact}), $P(c_t|x_{t-1},s_t,y_t)$
\begin{align}
P(c_t|x_{t-1},s_t,y_t)=& {1\over Z_C} \int db_t P(c_t,b_t,y_t|c_{t-1},b_{t-1},s_{t-1},s_t)
\end{align}
If $s_t=0$ then $P(c_t|x_{t-1},s_t=0,y_t)=\delta(c_t-c_{t-1}e^{-\lambda\delta t})$. The case $s_t=1$ need a little more algebra
\begin{align}
P(c_t|x_{t-1},s_t,y_t)=& {1\over Z_C} \int db_t \frac{\gamma_C}{\sqrt{2\pi\sigma_Y^2}\sqrt{2\pi\sigma_B^2}} \exp\left\lbrace -\gamma (c_t-c_{t-1}e^{-\lambda\delta t})-\frac{1}{2}\frac{(b_t-b_{t-1})^2}{\sigma_B^2}-\frac{1}{2}\frac{(y_t-b_t-c_t)^2}{\sigma_Y^2}\right\rbrace\nonumber\\
=& {1\over Z_C} \frac{\gamma_C}{\sqrt{2\pi(\sigma_Y^2+\sigma_B^2)}} \exp\left\lbrace -\gamma (c_t-c_{t-1}e^{-\lambda\delta t})-\frac{1}{2}\frac{(y_t-b_{t-1}-c_t)^2}{\sigma_Y^2+\sigma_B^2}\right\rbrace\nonumber\\
\propto & \exp\left\lbrace -\frac{\left(c_t+b_{t-1}-y_t + \gamma_C\sigma^2\right)^2}{2(\sigma_B^2+\sigma_Y^2)}\right\rbrace\mathbb{I}\left(c_t\ge c_{t-1}e^{-\lambda_C\delta t}\right)\label{eq:ccond}
\end{align}
which is a truncated gaussian distribution.
Finally, the conditional distribution of $b_t$ is
\begin{align}
P(b_t|x_{t-1},s_t,c_t,y_t) = \frac{1}{\sqrt{2\pi\sigma_Y^2}}e^{-\frac{(y_t-b_t-c_t)^2}{2\sigma_Y^2}}\label{eq:bcond}
\end{align}
So we can draw samples of $s_t$, $c_t$ and $b_t$ sequentially from $P(s_t,c_t,b_t|x_{t-1},y_t)$ as
\begin{enumerate}
\item draw $s_t$ from a Bernoulli distribution according to Eqs.~(\ref{eq:s0},\ref{eq:s1})
\item draw $c_t$ from a truncated gaussian with mean $y_t-b_{t-1}-\gamma_C\sigma^2$ and variance $(\sigma_B^2+\sigma_Y^2)$ as in Eq.~(\ref{eq:ccond}) with lower bound of $c_{t-1}e^{-\lambda\delta t}$
\item draw $b_t$ from a gaussian distribution with mean $y_t-c_t$ and variance $\sigma_Y^2$
\end{enumerate}
\section{Asymptotics of $Z_S$}
When $z_t$ in Eq.~(\ref{eq:ZS}) is very large we can use the asymptotic expansion of the error function 
\begin{align}
\mathrm{erfc}(z)\equiv \frac{2}{\sqrt{\pi}}\int_z^\infty dt e^{-t^2} = \frac{e^{-z^2}}{z\sqrt{\pi}}\left(1-\frac{1}{2z^2}+\mathcal{O}(z^{-4})\right)
\end{align}
so we can approximate $Z_S$ as
\begin{align}
Z_S=& \frac{(1-q)}{\sqrt{2\pi\sigma^2}} e^{ -\frac{z_t^2}{2\sigma^2}} \left[ 1+ 
\frac{q}{(1-q)}\frac{\gamma_C\sigma }{z_t/\sigma+\gamma_C\sigma}\right]
\end{align}
\section{Conditional distribution of parameters given latent trajectories}
So far we considered probabilities of latent and observed variables $p_\theta(\cdot)$ for fixed parameter set $\theta\equiv\lbrace \sigma_B,\sigma_Y,\gamma_C,q\rbrace$. For a given set of latent trajectories $x_t\equiv\lbrace s_t,c_t,b_t\rbrace$, we can also write the probability of the parameters as   

\begin{align}
P(\theta|x_t,y_t)\propto P(\theta)\cdot P_\theta(x_t,y_t)
\end{align}
where
\begin{align}
& P_\theta(x_t,y_t) \propto 
 q^M(1-q)^{T-M}\sigma_B^{-T+1}\sigma_Y^{-T} \gamma_C^M\exp\left(
                                            -\sum_{t=1}^{T}\frac{(y_t-c_t-b_t)^2}{2\sigma_Y^2}
                                            -\sum_{t=2}^{T}\frac{(b_t-b_{t-1})^2}{2\sigma_B^2}
                                            -\sum_{t=2}^{T}\gamma_C (c_t-c_{t-1}e^{-\lambda\delta t})s_t \right)
\end{align}
and $M=\sum_{t=1}^T s_t$ is the number of time frames where the neuron was in an active state. By choosing conjugate priors on the parameters as
\begin{align}
P(\sigma_Y^2)&=\mathrm{invGamma}(\sigma_Y^2;\alpha_Y,\beta_Y)\\
P(\sigma_B^2)&=\mathrm{invGamma}(\sigma_B^2;\alpha_B,\beta_B)\\
P(\gamma_C)&=\mathrm{Gamma}(q;\alpha_C,\beta_C)\\
P(q)&=\mathrm{Beta}(\alpha_q,\beta_q)
\end{align}
we can factorize the full conditional distribution on the parameters as the product of the following distributions
\begin{align}
P(\sigma_Y^2|x_t,y_t) & = \mathrm{invGamma}\left(\sigma_Y^2; T/2+\alpha_Y,{1\over 2}\sum_{t=1}^{T}(y_t-c_t-b_t)^2+\beta_Y\right)\\
P(\sigma_B^2|x_t,y_t) & = \mathrm{invGamma}\left(\sigma_B^2; (T-1)/2+\alpha_B,{1\over 2}\sum_{t=2}^{T}(b_t-b_{t-1})^2+\beta_B\right)\\
P(\gamma_C|x_t,y_t) & = \mathrm{Gamma}\left(\gamma_C; M +\alpha_C,\sum_{t=2}^{T}(c_t-c_{t-1}e^{\lambda\delta t})+\beta_C\right)\\
P(q|x_t,y_t) & = \mathrm{Beta}\left(q; M +\alpha_q,T-M+\beta_q\right)
\end{align}
\end{document}
