CC	=	g++ -std=c++11 -Wall -fPIC -fopenmp
OPENCV =  `pkg-config --libs opencv` 
UNAME = $(shell uname)
BINDIR=bin
OBJDIR=.obj
SRCDIR=src
SOURCESXX := $(wildcard $(SRCDIR)/*.cxx)
SOURCES  := $(wildcard $(SRCDIR)/*.cpp)
OBJECTS  := $(SOURCES:$(SRCDIR)/%.cpp=$(OBJDIR)/%.o)
EXECUTABLES := $(SOURCESXX:$(SRCDIR)/%.cxx=$(BINDIR)/%)

LIBS =  -L/usr/lib -lgsl -lblas -larmadillo -lGL -L/usr/local/lib/x86_64-linux-gnu #-lglut
INCS = -I /usr/local/include -I/usr/include

all: MAIN

MAIN: OPTIM_FLAGS := -O3 #O2 and O3 produce segfault
MAIN: DEBUG_FLAGS :=
MAIN: $(EXECUTABLES)

DEBUG: OPTIM_FLAGS := -O0 #for callgrind
DEBUG: DEBUG_FLAGS := -ggdb3 -g -lprofiler
DEBUG: $(EXECUTABLES) 


$(OBJECTS): $(OBJDIR)/%.o : $(SRCDIR)/%.cpp
	$(CC) $(CFLAGS) -c $< -o $@ $(LIBS) $(INCS) $(OPTIM_FLAGS)
$(EXECUTABLES): $(BINDIR)/% : $(SRCDIR)/%.cxx $(OBJECTS) 
	$(CC) -o $@ $< $(OBJECTS) $(LIBS) $(INCS) $(OPTIM_FLAGS) $(DEBUG_FLAGS)


.PHONY: all clean

clean:
	rm $(EXECUTABLES) 
	rm $(OBJECTS)


