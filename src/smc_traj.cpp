#include<iostream>
#include<gsl/gsl_rng.h>
#include <gsl/gsl_randist.h>
#include <gsl/gsl_cdf.h>
#include <gsl/gsl_math.h>
#include "../include/smc_traj.h"
#include "../include/calciumtrace.h"
#include "../include/usr_fun.h"
#include <fstream>
#include <algorithm>


smc_parameters::smc_parameters(){
    lambda=0.6;
    prob=0.05;
    sdX=0.1;
    sdC=1;
    sdB=0.1;
    freq=5;
    B0=1;
    C0=0;
}

double smc_parameters::prior(double z, double gv)
{
    return(exp(-(pow(sdX,2)+pow(sdC,2)+pow(sdB,2))/gv*z));
}

smc_traj::smc_traj(int seed=0){

    rng = gsl_rng_alloc (gsl_rng_mt19937);
    rngseed=seed;
    gsl_rng_set(rng,seed);
    sigmaB_prop=1e-4;
    sigmaC_prop=1e-4;
    sdB0_prop_mcmc=1e-3;
    sdC0_prop_mcmc=1e-3;
    gamma_sd=0.1;
    beta_N=3000;
    nparticles=0;
    datasize=0;
}

void smc_traj::InitParticles(size_t npart)
{
    nparticles=npart;
    // Initialize particles
    if(datasize>0){
        particles.resize(nparticles,CalciumTrace(datasize));
        parent.resize(nparticles,std::vector<int>(datasize));
        weights.resize(nparticles);
    } else {
        std::cout<<"Undefined datasize!"<<std::endl;
        std::exit(1);
    }
}

void smc_traj::setData(double *d, int n)
{
    data=d;
    datasize=n;
}


// This function needs to be rewritten because the elements of the vector
// particles do not represent single trajectory due to resampling.
void smc_traj::writeParticles()
{
    int i,k;
    std::ofstream output_cal("calcium.dat");
    std::ofstream output_sks("spikes.dat");
    std::ofstream output_b("baseline.dat");

    for(i=0;i<particles.size();++i){
        for(k=0;k<datasize;++k){
            output_b<<particles[i].baseline[k]<<' ';
        }
        for(k=0;k<datasize;++k){
            output_sks<<particles[i].spikes[k]<<' ';
        }
        for(k=0;k<datasize;++k){
            output_cal<<particles[i].calcium[k]<<' ';
        }

        output_b<<std::endl; output_cal<<std::endl; output_sks<<std::endl;

    }
}

double smc_traj::dproposal(smc_parameters &pnew, smc_parameters &pold)
{
    double logp=0;
    logp=gaussian_pdf_log(pnew.B0,pold.B0,sdB0_prop_mcmc)+
         gaussian_pdf_log(pnew.C0,pold.C0,sdC0_prop_mcmc)+
         log(gsl_ran_beta_pdf(pnew.prob,pold.prob*beta_N,(1-pold.prob)*beta_N));

    logp+= (pold.sdB>1) ? log(gsl_ran_gamma_pdf(pnew.sdB,pow(pold.sdB,2)/gamma_sd,gamma_sd/pold.sdB))
                        : log(gsl_ran_gamma_pdf(pnew.sdB,1./gamma_sd,pold.sdB*gamma_sd));

    logp+= (pold.sdC>1) ? log(gsl_ran_gamma_pdf(pnew.sdC,pow(pold.sdC,2)/gamma_sd,gamma_sd/pold.sdC))
                        : log(gsl_ran_gamma_pdf(pnew.sdC,1./gamma_sd,pold.sdC*gamma_sd));

    logp+= (pold.sdX>1) ? log(gsl_ran_gamma_pdf(pnew.sdX,pow(pold.sdX,2)/gamma_sd,gamma_sd/pold.sdX))
                        : log(gsl_ran_gamma_pdf(pnew.sdX,1./gamma_sd,pold.sdX*gamma_sd));

    return(exp(logp));
}

void smc_traj::rproposal(smc_parameters &pgen, smc_parameters &pold, int var)
{
    pgen=pold;
    switch (var){
    case 0:
        pgen.B0=pold.B0+gsl_ran_gaussian(rng,sdB0_prop_mcmc);
        break;
    case 1:
        pgen.C0=pold.C0+gsl_ran_gaussian(rng,sdC0_prop_mcmc);
        break;
    case 2:
        pgen.prob=gsl_ran_beta(rng,pold.prob*beta_N,(1-pold.prob)*beta_N);
        break;
    case 3:
    // Note that in GSL the gamma distribution is parameterized as gamma(rng,k=shape,theta=scale).
    // Larger values of the shape parameters at fixed mean=shape*scale make the distribution narrower
    // Note2: Importantly, to keep fixed the variance and the mean of the gamma we need to use the parameterization:
        // gamma(mu^2/sigma,mu/sigma)
        pgen.sdB=(pold.sdB>1) ? gsl_ran_gamma(rng,pow(pold.sdB,2)/gamma_sd,gamma_sd/pold.sdB) : gsl_ran_gamma(rng,1./gamma_sd,pold.sdB*gamma_sd);
        break;
    case 4:
        pgen.sdC=(pold.sdC>1) ? gsl_ran_gamma(rng,pow(pold.sdC,2)/gamma_sd,gamma_sd/pold.sdC) : gsl_ran_gamma(rng,1./gamma_sd,pold.sdC*gamma_sd);
        break;
    case 5:
        pgen.sdX=(pold.sdX>1) ? gsl_ran_gamma(rng,pow(pold.sdX,2)/gamma_sd,gamma_sd/pold.sdX) : gsl_ran_gamma(rng,1./gamma_sd,pold.sdX*gamma_sd);
        break;
    }
}

void smc_traj::runtest(std::vector<double> &F,smc_parameters& params)
{
    CalciumTrace trace(F.size());
    trace.calcium[0]=params.C0;
    trace.baseline[0]=params.B0;
    trace.spikes[0]=0;


    for(unsigned int t=1;t<trace.size;++t){
        trace.baseline[t]=trace.baseline[t-1]+gsl_ran_gaussian(rng,params.sdB);
        if(gsl_rng_uniform(rng)<params.prob){
            trace.spikes[t]=1;
            trace.calcium[t]=trace.calcium[t-1]*exp(-params.lambda/params.freq)+gsl_ran_gaussian(rng,params.sdC);
        } else {
            trace.spikes[t]=0;
            trace.calcium[t]=trace.calcium[t-1]*exp(-params.lambda/params.freq);
        }
     }

    F[0]=params.B0+params.C0;
    for(unsigned int t=1;t<trace.size;++t) F[t]=trace.baseline[t]+trace.calcium[t]+gsl_ran_gaussian(rng,params.sdX);
}

void smc_traj::SMC(smc_parameters & params)
{

    unsigned int i,j,k;
    double cnew,bnew,cspike,bspike;
    double logp0,logp1;
    double logp0_cumul, logp1_cumul;
    double alpha;
    double pi=M_PI;
    double varX=pow(params.sdX,2);
    double varC=pow(params.sdC,2);
    double varB=pow(params.sdB,2);
    std::vector<double> ml_vec(datasize);

    gsl_ran_discrete_t *resampling;
    std::vector<double> logw(nparticles);
    std::vector<double> maxlw_vec(datasize);


    for(i=0;i<nparticles;++i){
        particles[i].calcium[0]=params.C0;
        particles[i].baseline[0]=params.B0;
        particles[i].spikes[0]=0;
        weights[i]=1;
    }

    // Assign the first value of ML
    ml_vec[0]=gaussian_pdf_log(params.B0+params.C0-data[k],0,params.sdX);

    for(k=1;k<datasize;++k){
        gsl_ran_discrete_t *resampling = gsl_ran_discrete_preproc(nparticles, &weights[0]);
        for(j=0;j<nparticles;++j) parent[j][k]=gsl_ran_discrete(rng,resampling);
        gsl_ran_discrete_free(resampling);

        for(j=0;j<nparticles;++j){
            i=parent[j][k];
            cnew=particles[i].calcium[k-1]*exp(-params.lambda/params.freq); // dt=1/freq
            bnew=(varX*particles[i].baseline[k-1]+varB*(data[k]-cnew))/(varX+varB);
            // using Max likelihood
            //logp0=log(1-params.prob)-0.5*log(pow(2*pi,2))-0.5*log(varX)-0.5*log(varB)-pow(bnew-particles[i].baseline[k-1],2)/(2*varB)*(1+varX/varB);
            // using Marginal likelihood integrated over b
            logp0=log(1-params.prob)-0.5*log(2*pi)-0.5*log(varX+varB)-0.5*pow(data[k]-cnew-particles[i].baseline[k-1],2)/(varX+varB);

            cspike=(varC*(data[k]-particles[i].baseline[k-1])+cnew*(varX+varB))/(varX+varC+varB);
            bspike=particles[i].baseline[k-1]+varB/varC*(cspike-cnew);
            // Using Max likelihood
            //logp1=log(params.prob)-0.5*log(pow(2*pi,3))-0.5*log(varX)-0.5*log(varB)-0.5*log(varC)-pow(bspike-particles[i].baseline[k-1],2)/(2*varB)*(1+varX/varB+varC/varB);
            // Using Marginal likelihood integrated over calcium[k] and baseline[k]
            logp1=log(params.prob)-0.5*log(2*pi)-0.5*log(varX+varB+varC)-0.5*pow(data[k]-cnew-particles[i].baseline[k-1],2)/(varX+varB+varC);

            alpha=std::min(exp(logp1-logp0),1.0);

            //std::cout<<alpha<<std::endl;

            // here we propose
            if(gsl_rng_uniform(rng)<alpha){
                particles[j].baseline[k]=bspike+gsl_ran_gaussian(rng,sigmaB_prop);
                particles[j].calcium[k]=cspike+gsl_ran_gaussian(rng,sigmaC_prop);
                particles[j].spikes[k]=1;
            } else {
                particles[j].baseline[k]=bnew+gsl_ran_gaussian(rng,sigmaB_prop);
                particles[j].calcium[k]=cnew;
                particles[j].spikes[k]=0;
            }

            logw[j]=gaussian_pdf_log(particles[j].baseline[k]+particles[j].calcium[k]-data[k],0,params.sdX);
            if(particles[j].spikes[k]==1){

                logw[j]+=gaussian_pdf_log(particles[j].calcium[k]-cnew,0,params.sdC)-
                       gaussian_pdf_log(particles[j].calcium[k]-cspike,0,sigmaC_prop)+

                        gaussian_pdf_log(particles[j].baseline[k]-particles[i].baseline[k-1],0,params.sdB)-
                        gaussian_pdf_log(particles[j].baseline[k]-bspike,0,sigmaB_prop) +

                        log(params.prob);
            } else {

                logw[j]+=
                        gaussian_pdf_log(particles[j].baseline[k]-particles[i].baseline[k-1],0,params.sdB)-
                        gaussian_pdf_log(particles[j].baseline[k]-bnew,0,sigmaB_prop) +

                        log(1-params.prob);

            }

            if(alpha>0 && alpha<1) logw[j]-= log(alpha)*particles[j].spikes[k]+log(1-alpha)*(1-particles[j].spikes[k]);

            //if(weights[j]!=weights[j]) std::cout<<j<<' '<<particles[j].spikes[k]<<std::endl;


        }

        wfrom_logw(logw,weights,maxlw_vec[k]);

        ml_vec[k]=0;
        for(j=0;j<nparticles;j++) ml_vec[k]+=weights[j];

    }

    double LogMarginalLikelihood=ml_vec[0]; // this is already a log!
    for(k=1;k<datasize;k++) LogMarginalLikelihood+=log(ml_vec[k]);
    for(k=1;k<datasize;k++) LogMarginalLikelihood+=maxlw_vec[k];
    MarginalLikelihood=exp(LogMarginalLikelihood);

}

void smc_traj::MCMC(std::vector<smc_parameters> &parlist, std::vector<double> &d, size_t npart, size_t niter, std::ofstream &OUTFILE)
{

    unsigned int i,j,k;
    double ML_old;
    double alpha;
    double prior_new, prior_old;
    smc_parameters pnew;
    parlist.resize(niter);
    parlist[0]=pnew;

    //Calculate data variance
    double data_mean=0;
    double data_var=0;
    for(k=0;k<d.size();++k) data_mean+=d[k]/d.size();
    for(k=0;k<d.size();++k) data_var+=pow(d[k]-data_mean,2)/d.size();


    // This routine performs the MCMC over the parameters of the model.
    setData(&d[0],d.size());
    InitParticles(npart);
    SMC(parlist[0]);
    ML_old=MarginalLikelihood;
    prior_old=pnew.prior(1,data_var);

    for(i=1;i<niter;++i){

        // update of parameters
        rproposal(pnew,parlist[i-1],gsl_rng_uniform_int(rng,6));

        std::cout<<i<<std::endl;


        // recalculate ML
        SMC(pnew);
        prior_new=pnew.prior(1,data_var);

        alpha=MarginalLikelihood/ML_old * dproposal(parlist[i-1],pnew)/dproposal(pnew,parlist[i-1]) * prior_new/prior_old;
        if(gsl_rng_uniform(rng)<alpha){
            parlist[i]=pnew;
            ML_old=MarginalLikelihood;
            prior_old=prior_new;
        } else {
            parlist[i]=parlist[i-1];
        }



        OUTFILE<<parlist[i].B0<<' '
              <<parlist[i].C0<<' '
             <<parlist[i].prob<<' '
            <<parlist[i].sdX<<' '
           <<parlist[i].sdC<<' '
          <<parlist[i].sdB<<std::endl;


    }

}

void smc_traj::MaxML(std::vector<smc_parameters> &parlist, std::vector<double> &d, size_t npart, size_t niter, std::ofstream &OUTFILE)
{

    unsigned int i,j,k;
    double ML_old;
    double alpha;
    smc_parameters pnew;
    parlist.resize(niter);
    parlist[0]=pnew;

    // This routine performs the MCMC over the parameters of the model.
    setData(&d[0],d.size());
    InitParticles(npart);
    SMC(parlist[0]);
    ML_old=MarginalLikelihood;

    i=1;
    while(i<niter){
        std::cout<<i<<' '<<ML_old<<std::endl;

        // update of parameters
        rproposal(pnew,parlist[i-1],gsl_rng_uniform_int(0,5));

        // recalculate ML
        SMC(pnew);
        alpha=MarginalLikelihood/ML_old;
        if(alpha>1){
            parlist[i]=pnew;
            ML_old=MarginalLikelihood;
            OUTFILE<<parlist[i].B0<<' '
                  <<parlist[i].C0<<' '
                  <<parlist[i].prob<<' '
                  <<parlist[i].sdX<<' '
                  <<parlist[i].sdC<<' '
                 <<parlist[i].sdB<<std::endl;
            i++;
        }

    }

}
