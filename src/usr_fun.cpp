#include "../include/usr_fun.h"
#include <gsl/gsl_math.h>
#include <algorithm>
#include <iostream>
#include "../include/smc_exp.h"

double gaussian_pdf_log(double x,double mean,double sd){
    return -0.5*log(2*M_PI*pow(sd,2))-0.5*pow((x-mean)/sd,2);
}

void wfrom_logw(std::vector<double> &lw,std::vector<double> &w,double &maxlw){

    // M: can I assume that if we have NAs in there something like max_element or exp will throw an error?
    // I want to avoid the lw[i]!=lw[i] in the loop ...
    try
    {
        maxlw=*(std::max_element(lw.begin(),lw.end()));

        #ifdef _OPENMP
        #pragma omp parallel for
        #endif
        for(unsigned int i=0;i<lw.size();++i){

            w[i]=exp(lw[i]-maxlw);

        }   
    }
    catch(const std::exception& e)
    {
        std::cout<<"wfrom_logw: problems with the input"<<std::endl;
        std::cerr << e.what() << '\n';
        abort();
    }
    catch(...)
    {
        std::cout<<"wfrom_logw: problems with the input"<<std::endl;
        abort();
    }
    
}

void SMCsq(std::vector<smc_exp_parameters> &parlist, std::vector<double> &d, size_t npart_theta, size_t npart_X, std::ofstream &OUTFILE);
