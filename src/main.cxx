#include<iostream>
#include<armadillo>
#include "../include/smc_traj.h"
#include "../include/smc_exp.h"
#include "../include/calciumtrace.h"
#include <fstream>

using namespace std;

int main(int argc, char* argv[]){

    bool useDATA = false;
    int datasize=5000;
    size_t particles=1000, chainlen=1000;

    // **** Read and interpret command line
    int na = 1;
    while(na < argc)
    {
		if ( 0 == std::string{argv[na]}.compare(std::string{"--useData"}) )
		{
			useDATA = true;

			if (na+1==argc) break; // in case it's last, break
			++na; // otherwise augment counter
		}
		else if ( 0 == std::string{argv[na]}.compare(std::string{"--nIter"}) )
		{
			chainlen = std::stoi(argv[++na]); // use the next
			if (na+1==argc) break;
			++na;
		}
		else if ( 0 == std::string{argv[na]}.compare(std::string{"--nXParticles"}) )
		{
			particles = std::stoi(argv[++na]);
			if (na+1==argc) break;
			++na;
		}
		else if ( 0 == std::string{argv[na]}.compare(std::string{"--dataLength"}) )
		{
			datasize = std::stoi(argv[++na]); 
			if (na+1==argc) break;
			++na;
		}
		else
		{
			std::cout << "Unknown option: " << argv[na] << std::endl;
			return(1); //this is exit if I'm in a function elsewhere
    	}
    }//end reading from command line

    // Write the input down to have them in R
    ofstream sampler_parameters("sampler_parameters.dat");
    sampler_parameters << "dataLength" << " " << "nXParticles" << " " << "nIter" << std::endl <<
                        datasize    << " " << particles     << " " << chainlen << std::endl;
    sampler_parameters.close();



    // Files could be parameters as well, but for now this will do
    ofstream outputfile("out.dat");
    ofstream paramfile("chain.dat");
    ifstream trajfile("/home/diana/workspace/Dominic/181203_f3_grin/181203_f3_grin.Fraw");
    //ifstream trajfile("Good_traces.dat");
    vector<double> traj_example;
    smc_exp_parameters params_exp;
    std::vector<smc_exp_parameters> parlist_exp;

    double read;
    string line;
    
    long long nRNGDraws = datasize*particles*chainlen*5; // quite random...rather, arbitrary

    // M: My laptop has 8 logical threads, but if I run with 8 threads there's actually
    // a slowdown due to openMP overhead, so I'll stick to 6. You can change it as needed
    std::vector<long long> seed = {0,nRNGDraws
        ,2*nRNGDraws,3*nRNGDraws
        ,4*nRNGDraws,5*nRNGDraws
        // ,6*nRNGDraws,7*nRNGDraws 
        };
        
    smc_exp sampler(seed);
    
    sampler.fromDATA=useDATA;
    CalciumTrace Orig(datasize);

    if(useDATA){
        // read from file
        for(unsigned int i=0;i<20;++i) getline(trajfile,line); // the top i is the cellID(starting from id=1) to process
        while(trajfile>>read && traj_example.size()<datasize) traj_example.push_back(read);
    } else {

        // look for an init_parameters file
        ifstream init_par("init_parameters.dat");
        init_par.exceptions(std::ifstream::failbit | std::ifstream::badbit); // this helps by throwing exceptions if the 
        if( init_par )
        {
            try
            {
                double num;
                init_par >> params_exp.B0;
                init_par >> params_exp.sdB;
                init_par >> params_exp.gammaC;
                init_par >> params_exp.sdX;
                init_par >> params_exp.prob[0];
                init_par >> params_exp.prob[1];

            }    
            catch(const std::exception& e)
            {
                std::cerr << "Something went wrong while reading the init_parameters.dat file, " <<
                            "be sure to follow the specifications in README.md" << std::endl;
                std::cerr << e.what() << '\n';
                return 1;
            }
        }
        else
        {
            // default values
            params_exp.B0=1;
            params_exp.sdB=0.2;
            params_exp.gammaC=0.1;
            params_exp.sdX=0.2;
            params_exp.prob[0]=0.1;
            params_exp.prob[1]=0.1;
        }

        traj_example.resize(datasize);
        sampler.runtest(traj_example,params_exp,Orig);
    }

    for(unsigned int k=0;k<traj_example.size();++k){

        outputfile<<traj_example[k]<<' ';
        if(!useDATA){
            outputfile<<Orig.spikes[k]<<' '
                      <<Orig.baseline[k]<<' '
                      <<Orig.calcium[k];
        }
        outputfile<<std::endl;
    }

    outputfile.close();

    //sampler.MCMC(parlist_exp,traj_example,particles,chainlen,paramfile);
    sampler.pGibbs(parlist_exp,traj_example,particles,chainlen,paramfile);
    paramfile.close();

    return 0;

}
