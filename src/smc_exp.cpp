// This class implements the SMC+MCMC algorithm for inference of parameters
// in the exponential model for calcium activity.

#include<iostream>
#include<gsl/gsl_rng.h>
#include <gsl/gsl_randist.h>
#include <gsl/gsl_cdf.h>
#include <gsl/gsl_math.h>
#include <gsl/gsl_sf.h>
#include "../include/smc_exp.h"
#include "../include/calciumtrace.h"
#include "../include/usr_fun.h"
#include <fstream>
#include <algorithm>

#ifdef _OPENMP
   #include <omp.h>
#else
   #define omp_get_max_threads() 1
   #define omp_get_thread_num() 0
#endif

// the smc_exp_parameters class is the parameter class for the SMC+MCMC
// It includes parameters that are fixed and parameters to be estimated.
smc_exp_parameters::smc_exp_parameters(){
    // constants
    lambda=0.6;
    freq=5;
    dt=1.0/freq;

    // variable parameters
    prob = { 0.01 , 0.01 }; // prob[0]: spike likelihood given no spike at time t-1 , prob[1]: spike likelihood given spike at time t-1
                         
    sdX=.1;
    tdist_nu=3;
    gammaC=.1;
    sdB=1e-1;
    sdB0=1;
    B0=1;
    C0=0;
}

void smc_exp_parameters::print(){
    std::cout<<"lambda = "<<lambda<<std::endl;
    std::cout<<"prob = "<<prob[0]<<' '<<prob[1]<<std::endl;
    std::cout<<"sdX = "<<sdX<<std::endl;
    std::cout<<"gammaC = "<<gammaC<<std::endl;
    std::cout<<"sdB = "<<sdB<<std::endl;
    std::cout<<"freq = "<<freq<<std::endl;
    std::cout<<"B0 = "<<B0<<std::endl;
    std::cout<<"C0 = "<<C0<<std::endl;
}

smc_exp_parameters::smc_exp_parameters(const smc_exp_parameters &par) :
    sdX(par.sdX),
    sdB(par.sdB),
    B0(par.B0),
    C0(par.C0),
    gammaC(par.gammaC),
    lambda(par.lambda),
    freq(par.freq),
    prob({ par.prob[0] , par.prob[1] })
{ }

smc_exp_parameters smc_exp_parameters::operator=(const smc_exp_parameters &par)
{
    C0=par.C0;
    B0=par.B0;
    sdX=par.sdX;
    sdB=par.sdB;
    gammaC=par.gammaC;
    prob = { par.prob[0] , par.prob[1] }; 

    lambda=par.lambda;
    freq=par.freq;

    return *this;
}

// The prior distribution on the variances is just an exponential to penalize
// high values of the variances
double smc_exp_parameters::prior(PARTYPE p)
{
    double logprior;

    switch(p){
      case PARTYPE::B0:
        logprior = log(gsl_ran_gamma_pdf(B0, 1, 5));
        break;
    case PARTYPE::C0:
        logprior = log(gsl_ran_gamma_pdf(C0,1,5));
        break;
    case PARTYPE::SDX:
        logprior = log(gsl_ran_gamma_pdf(sdX,1,5));
        break;
    case PARTYPE::SDB:
        logprior = gaussian_pdf_log(log(sdB),log(1e-1),3);
        break;
    case PARTYPE::GAMMAC:
        logprior = log(gsl_ran_gamma_pdf(gammaC,1,5));
        break;
    case PARTYPE::PROB0:
        logprior = log(gsl_ran_beta_pdf(prob[0],1,100));
        break;
    case PARTYPE::PROB1:
        logprior = log(gsl_ran_beta_pdf(prob[1],1,100));
        break;
    }

    return(logprior);
}

void smc_exp_parameters::rprior(const gsl_rng * rng)
{

        B0 = gsl_ran_gamma(rng,1, 5);
        C0 = 0;

        sdX = gsl_ran_gamma(rng,1,5);
        sdB = exp(log(1e-1)+gsl_ran_gaussian(rng,3));
        gammaC = gsl_ran_gamma(rng,1,5);
        prob[0] = gsl_ran_beta(rng,1,100);

}

// main constructor of the smc_exp class
smc_exp::smc_exp( std::vector<long long> seed ){

    // each instance of smc_exp contains its random number generator
    
    size_t nThreads = std::min( (int) seed.size() , omp_get_max_threads() );

    #ifdef _OPENMP
		omp_set_num_threads( nThreads ); 
    #endif

    rng = std::vector<gsl_rng*>( nThreads );
    rngseed = std::vector<long long>( nThreads );

    for( unsigned int t=0; t<nThreads; ++t)
    {
        rng[t] = gsl_rng_alloc (gsl_rng_mt19937);
        rngseed[t]=seed[t];
        gsl_rng_set(rng[t],seed[t]);
    }

    // parameters for SMC proposals
    sigmaB_prop=1;
    shapeC_prop=1;
    nparticles=0;

    // parameters for MCMC proposals
    sdB0_prop_mcmc=1;
    gammaC0_prop_mcmc=10;
    gamma_shape=30;
    beta_N=20;
    datasize=0;

    // Conjugate priors parameters for particle Gibbs sampler
    alphaX=2;
    betaX=10;

    alphaC=2;
    betaC=10;

    alphaB=3;
    betaB=10;

    alphaq=1;
    betaq=100;

    prior_sdB0=1;
    alphaB0=1;
    betaB0=1;

}

// This initializes the particles by resizing the particle vector
void smc_exp::InitParticles(size_t npart)
{
    nparticles=npart;
    // Initialize particles
    if(datasize>0){
        particles.resize(nparticles,CalciumTrace(datasize));
        parent.resize(nparticles,std::vector<int>(datasize));
        weights.resize(nparticles);
    } else {
        std::cout<<"Undefined datasize!"<<std::endl;
        std::exit(1);
    }
}

// This function assign the data to be used for inference
void smc_exp::setData(double *d, int n)
{
    data=d;
    datasize=n;
}


// This function writes the output of the SMC. It reconstruct the trajectory from the parent vector.
void smc_exp::writeParticles()
{
    int i,k;
    int ipar;
    std::ofstream output_cal("calcium.dat");
    std::ofstream output_sks("spikes.dat");
    std::ofstream output_b("baseline.dat");
    std::ofstream output_weights("weights.dat");

    // Use a tmp calciumtrace to reconstruct the trajectory of each particle
    CalciumTrace tmp(datasize);

    for(i=0;i<particles.size();++i){
        ipar=i;
        k=datasize;
        while(k>0){
            k--;
            tmp.baseline[k]=particles[ipar].baseline[k];
            tmp.calcium[k]=particles[ipar].calcium[k];
            tmp.spikes[k]=particles[ipar].spikes[k];
            ipar=parent[ipar][k];
        }
        for(k=0;k<datasize;++k){
            output_b<<tmp.baseline[k]<<' ';
            output_cal<<tmp.calcium[k]<<' ';
            output_sks<<tmp.spikes[k]<<' ';
        }

        output_weights<<weights[i]<<std::endl;
        output_b<<std::endl; output_cal<<std::endl; output_sks<<std::endl;

    }
}

void smc_exp::writeSampledParticle(unsigned int i, CalciumTrace& tmp)
{
    int k;
    int ipar;


    // Use a tmp calciumtrace to reconstruct the trajectory of each particle    

    ipar=i;
    k=tmp.size;
    while(k>0){
        k--;
        tmp.baseline[k]=particles[ipar].baseline[k];
        tmp.calcium[k]=particles[ipar].calcium[k];
        tmp.spikes[k]=particles[ipar].spikes[k];
        ipar=parent[ipar][k];
    }    
}

// pdf of the proposal function for the MCMC algorithm over the parameters
double smc_exp::dproposal(smc_exp_parameters &pnew, smc_exp_parameters &pold,PARTYPE var)
{
    double logp=0;

    std::cout<<"eval: "<<(int)var<<std::endl;
    std::cout<<pold.B0<<' '<<pold.C0<<' '<<pold.prob[0]<<' '<<pold.sdB<<' '<<pold.gammaC<<' '<<pold.sdX<<' '<<pold.prob[1]<<std::endl;
    std::cout<<pnew.B0<<' '<<pnew.C0<<' '<<pnew.prob[0]<<' '<<pnew.sdB<<' '<<pnew.gammaC<<' '<<pnew.sdX<<' '<<pnew.prob[1]<<std::endl;


    switch(var){
    case PARTYPE::B0:
        logp=gaussian_pdf_log(pnew.B0,pold.B0,sdB0_prop_mcmc);
            // B0 is normally distributed around the old B0 value with sd=sdB0_prop_mcmc
        break;
    case PARTYPE::C0:
        logp=log(gsl_ran_gamma_pdf(pnew.C0,gamma_shape,pnew.C0/gamma_shape));
        break;
    case PARTYPE::PROB0:
        logp=//log(gsl_ran_beta_pdf(pnew.prob[0],pold.prob[0]*beta_N,(1-pold.prob[0])*beta_N));
             log(gsl_ran_gamma_pdf(pnew.prob[0],gamma_shape,pnew.prob[0]/gamma_shape));
            // the spike probability is distributed as a beta with beta_N controlling the width of
            // the distribution.
        break;
    case PARTYPE::SDB:
        logp= log(gsl_ran_gamma_pdf(pnew.sdB,gamma_shape,pold.sdB/gamma_shape));
        break;
    // The way I am using the gamma distribution is such that for pold.sdB>1 the standard deviation is constant
    // and equal to gamma_sd. For sdB<1 the variance of the gamma distribution scales down.
    // with this parameterization, gamma_sd must be larger than 1.
    case PARTYPE::GAMMAC:
        logp = log(gsl_ran_gamma_pdf(pnew.gammaC,gamma_shape,pold.gammaC/gamma_shape));
        break;
    case PARTYPE::SDX:
        logp = log(gsl_ran_gamma_pdf(pnew.sdX,gamma_shape,pold.sdX/gamma_shape));
        break;
    case PARTYPE::PROB1:
        logp=log(gsl_ran_beta_pdf(pnew.prob[1],pold.prob[1]*beta_N,(1-pold.prob[1])*beta_N));
        break;
    case PARTYPE::ALL:
        logp=// B0
                gaussian_pdf_log(pnew.B0,pold.B0,sdB0_prop_mcmc)+
                // C0
                log(gsl_ran_gamma_pdf(pnew.C0,gamma_shape,pold.C0/gamma_shape))+
                // prob0
                //+log(gsl_ran_beta_pdf(pnew.prob[0],pold.prob[0]*beta_N,(1-pold.prob[0])*beta_N))+
                log(gsl_ran_gamma_pdf(pnew.gammaC,gamma_shape,pold.gammaC/gamma_shape))+
                // sdB
                log(gsl_ran_gamma_pdf(pnew.sdB,gamma_shape,pold.sdB/gamma_shape))+
                // gammaC
                log(gsl_ran_gamma_pdf(pnew.gammaC,gamma_shape,pold.gammaC/gamma_shape))+
                // sdX
                log(gsl_ran_gamma_pdf(pnew.sdX,gamma_shape,pold.sdX/gamma_shape))+
                // prob1
                log(gsl_ran_beta_pdf(pnew.prob[1],pold.prob[1]*beta_N,(1-pold.prob[1])*beta_N));
        break;
    }

    return(exp(logp));
}

// random generator of the new parameter values.
void smc_exp::rproposal(smc_exp_parameters &pgen, smc_exp_parameters &pold, PARTYPE var)
{
    pgen=pold;
//    std::cout<<"pold: "<<var<<std::endl;
//    std::cout<<pold.B0<<' '<<pold.C0<<' '<<pold.prob[0]<<' '<<pold.sdB<<' '<<pold.gammaC<<' '<<pold.sdX<<' '<<pold.prob[1]<<std::endl;

    switch (var){
    case PARTYPE::B0:
        pgen.B0=pold.B0+gsl_ran_gaussian(rng[omp_get_thread_num()],sdB0_prop_mcmc);
        break;
    case PARTYPE::C0:
        pgen.C0=gsl_ran_gamma(rng[omp_get_thread_num()],gamma_shape,pold.C0/gamma_shape);
        break;
    case PARTYPE::PROB0:
        //pgen.prob[0]= gsl_ran_beta(rng[omp_get_thread_num()],pold.prob[0]*beta_N,(1-pold.prob[0])*beta_N);
        pgen.prob[0]= gsl_ran_gamma(rng[omp_get_thread_num()],gamma_shape,pold.prob[0]/gamma_shape);
        break;
    case PARTYPE::SDB:
    // Note that in GSL the gamma distribution is parameterized as gamma(rng[omp_get_thread_num()],k=shape,theta=scale).
    // Larger values of the shape parameters at fixed mean=shape*scale make the distribution narrower
    // Note2: Importantly, to keep fixed the variance and the mean of the gamma we need to use the parameterization:
        // gamma(mu^2/sigma,mu/sigma)
        pgen.sdB=gsl_ran_gamma(rng[omp_get_thread_num()],gamma_shape,pold.sdB/gamma_shape);
        break;
    case PARTYPE::GAMMAC:
        pgen.gammaC=gsl_ran_gamma(rng[omp_get_thread_num()],gamma_shape,pold.gammaC/gamma_shape);
        break;
    case PARTYPE::SDX:
        pgen.sdX=gsl_ran_gamma(rng[omp_get_thread_num()],gamma_shape,pold.sdX/gamma_shape);
        break;
    case PARTYPE::ALL:
        pgen.B0=pold.B0+gsl_ran_gaussian(rng[omp_get_thread_num()],sdB0_prop_mcmc);
        pgen.C0=gsl_ran_gamma(rng[omp_get_thread_num()],gamma_shape,pgen.C0/gamma_shape);
        pgen.prob[0]= gsl_ran_beta(rng[omp_get_thread_num()],pold.prob[0]*beta_N,(1-pold.prob[0])*beta_N);
        pgen.sdB=gsl_ran_gamma(rng[omp_get_thread_num()],gamma_shape,pold.sdB/gamma_shape);
        pgen.gammaC=gsl_ran_gamma(rng[omp_get_thread_num()],gamma_shape,pold.gammaC/gamma_shape);
        pgen.sdX=gsl_ran_gamma(rng[omp_get_thread_num()],gamma_shape,pold.sdX/gamma_shape);
        break;
    }
}

void smc_exp::rGibbsParams(smc_exp_parameters& p, const CalciumTrace &X){

    int M=0;
    double squaresC=0,squaresX=0, squaresB=0;

    for(unsigned int i=1;i<X.size;++i){
        if(X.spikes[i]>0){
            M++;
            squaresC+=X.calcium[i]-X.calcium[i-1]*exp(-p.lambda*p.dt);
        }
        squaresB+=pow(X.baseline[i]-X.baseline[i-1],2);
    }

    for(unsigned int i=0;i<X.size;++i){
        squaresX+=pow(data[i]-X.baseline[i]-X.calcium[i],2);
    }

    p.C0     = X.calcium[0];
    p.prob[0]= gsl_ran_beta(rng[omp_get_thread_num()],M+alphaq,X.size-M+betaq)/p.dt;
    p.sdB    = pow(gsl_ran_gamma(rng[omp_get_thread_num()],0.5*(datasize-1) + alphaB, 1./(0.5*squaresB+betaB)),-0.5);
    p.sdX    = pow(gsl_ran_gamma(rng[omp_get_thread_num()],0.5*datasize + alphaX, 1./(0.5*squaresX+betaB)),-0.5);
    p.gammaC = gsl_ran_gamma(rng[omp_get_thread_num()],M + alphaC, 1/(squaresC+betaC));
    p.sdB0   = pow(gsl_ran_gamma(rng[omp_get_thread_num()],0.5+alphaB0, 1/(0.5*pow(X.baseline[0]-p.B0,2)+betaB0)),-0.5);
    p.B0     = gsl_ran_gaussian(rng[omp_get_thread_num()],pow(1./pow(p.sdB0,2)+1./pow(prior_sdB0,2),-1))+
               (1./pow(p.sdB0,2)*p.B0 + 1./pow(prior_sdB0,2))/pow(1./pow(p.sdB0,2)+1./pow(prior_sdB0,2),-1);

}

// This function generates a random trajectory using a given set of parameters
void smc_exp::runtest(std::vector<double> &F,smc_exp_parameters& params, CalciumTrace& trace)
{

    trace.calcium[0]=params.C0;
    trace.baseline[0]=gsl_ran_gaussian(rng[omp_get_thread_num()],params.sdB0)+params.B0;
    trace.spikes[0]=0;

    for(unsigned int t=1;t<trace.size;++t){
        trace.baseline[t]=trace.baseline[t-1]+gsl_ran_gaussian(rng[omp_get_thread_num()],params.sdB);
        if(gsl_rng_uniform(rng[omp_get_thread_num()])<params.prob[trace.spikes[t-1]]*params.dt){
            trace.spikes[t]=1;
            trace.calcium[t]=trace.calcium[t-1]*exp(-params.lambda*params.dt)+gsl_ran_exponential(rng[omp_get_thread_num()],1./params.gammaC);
        } else {
            trace.spikes[t]=0;
            trace.calcium[t]=trace.calcium[t-1]*exp(-params.lambda*params.dt);
        }
     }

    for(unsigned int t=0;t<trace.size;++t) F[t]=trace.baseline[t]+trace.calcium[t]+gsl_ran_gaussian(rng[omp_get_thread_num()],params.sdX);

}

double smc_exp::Dyt_xtm1(double y1, int s0, double b0, double c0, const smc_exp_parameters & params)
{
    double sigma=sqrt(pow(params.sdB,2)+pow(params.sdX,2));
    double c0decay = c0*exp(-params.lambda*params.dt);
    double zt= c0decay-y1+b0;
    double ztt = zt/sigma+params.gammaC*sigma;
    double logZS;

    if(ztt<8 && ztt>-8){
        logZS = log(1-params.prob[0]*params.dt)-log(sqrt(2*M_PI)*sigma) - 0.5*pow(zt/sigma,2) +
            log( 1 + params.prob[0]*params.dt/(1-params.prob[0]*params.dt)*sqrt(M_PI/2)*params.gammaC*sigma*exp(0.5*pow(ztt,2))*gsl_sf_erfc(ztt/sqrt(2)));

    } else if (zt>=8) {
        logZS = log(1-params.prob[0]*params.dt)-log(sqrt(2*M_PI)/sigma) - 0.5*pow(zt/sigma,2);

    } else if (zt<=-8){
        logZS = log(params.prob[0]*params.dt*params.gammaC) + params.gammaC*zt+0.5*pow(params.gammaC*sigma,2);

    }

    return(logZS);
}

void smc_exp::Rsbc_xtm1_yt(int &s1, double &b1, double &c1, double &logZS, int s0, double b0, double c0, double y1, const smc_exp_parameters & params)
{
    double sigma=sqrt(pow(params.sdB,2)+pow(params.sdX,2));
    double c0decay = c0*exp(-params.lambda*params.dt);
    double zt= c0decay-y1+b0;
    double ztt = zt/sigma+params.gammaC*sigma;
    double P0;

    if(ztt<8 && ztt>-8){
        logZS = log(1-params.prob[0]*params.dt)-log(sqrt(2*M_PI)*sigma) - 0.5*pow(zt/sigma,2) +
            log( 1 + params.prob[0]*params.dt/(1-params.prob[0]*params.dt)*sqrt(M_PI/2)*params.gammaC*sigma*exp(0.5*pow(ztt,2))*gsl_sf_erfc(ztt/sqrt(2)));
        P0 = (1-params.prob[0]*params.dt)/sqrt(2*M_PI)/sigma * exp(-0.5*pow(zt/sigma,2)) / exp(logZS);
    } else if (zt>=8) {
        logZS = log(1-params.prob[0]*params.dt)-log(sqrt(2*M_PI)/sigma) - 0.5*pow(zt/sigma,2);
        P0 = 1;
    } else if (zt<=-8){
        logZS = log(params.prob[0]*params.dt*params.gammaC) + params.gammaC*zt+0.5*pow(params.gammaC*sigma,2);
        P0 = 0;
    }


    if(logZS!=logZS) {
        bool a = logZS!=logZS;
        std::cout<<"Rsbc_xtm1_yt: logZS = "<<logZS<<"  "<< a << std::endl;
        std::cout<<"  breaking down logZS:"<<std::endl;
        std::cout<<"    ztt = "<<ztt<<std::endl;
        std::cout<<"    erfc = "<<gsl_sf_erfc(ztt/sqrt(2))<<std::endl;
        std::cout<<"    P0 = "<<P0<<std::endl;
        std::cout<<"    part1 = "<<params.prob[0]*params.dt/(1-params.prob[0]*params.dt)<<std::endl;
        std::cout<<"    part2 = "<<sqrt(M_PI/2)*params.gammaC*sigma<<std::endl;
        std::cout<<"    part3 = "<<0.5*pow(ztt,2)<<std::endl;
        abort();
    }

    s1 = (gsl_rng_uniform(rng[omp_get_thread_num()]) < P0) ? 0 : 1;

    if(s1==0){
        c1=c0decay;
    } else {
        c1 = c0decay;
        int counter=0;
        while(c1<=c0decay && counter<500) {
            counter++;
            c1 = y1-b0-params.gammaC*sigma+gsl_ran_gaussian(rng[omp_get_thread_num()],sigma);
            //std::cout<<c0decay<<' '<<c1<<std::endl;
        }
        if(counter>500) c1=c0decay;
    }
    //std::cout<<"GOT!"<<std::endl;
    b1 = y1-c1+gsl_ran_gaussian(rng[omp_get_thread_num()],params.sdX);

}

// Implementation of particle filter test
void smc_exp::SMC(smc_exp_parameters & params,CalciumTrace &X,double &LM)
{

    unsigned int i,j,k;

    std::vector<double> ml_vec(datasize);

    gsl_ran_discrete_t *resampling;
    std::vector<double> logw(nparticles);
    std::vector<double> maxlw_vec(datasize);

    for(i=0;i<nparticles;++i){
        particles[i].calcium[0]=params.C0;
        particles[i].baseline[0]=params.B0;
        particles[i].spikes[0]=0;
        weights[i]=1;
    }

    // Here we use as proposal for X_1
    // q(x_1|y_1) = \delta(b_1-b_0)\delta(c_1-c_0)

    // All particles initially have the same weight equal to g(y_1|x_1) therefore the
    // average weight which contributes to the first term in the marginal likelihood is
    ml_vec[0]=gaussian_pdf_log(params.B0+params.C0-data[0],0,params.sdX);

    for(k=1;k<datasize;++k)
    {
        double logZS;
        resampling = gsl_ran_discrete_preproc(nparticles, &weights[0]);
        for(j=0;j<nparticles;++j) parent[j][k]=gsl_ran_discrete(rng[omp_get_thread_num()],resampling);
        gsl_ran_discrete_free(resampling);

        for(j=0;j<nparticles;++j){
            //std::cout<<j<<std::endl;

            i=parent[j][k];

            // Draw s_t from P(s_t|x_{t-1},y_t)
            Rsbc_xtm1_yt(particles[j].spikes[k],particles[j].baseline[k],particles[j].calcium[k],logZS,
                         particles[i].spikes[k-1],particles[i].baseline[k-1],particles[i].calcium[k-1],
                         data[k], params);

            logw[j]=logZS;

        }

        wfrom_logw(logw,weights,maxlw_vec[k]);

        ml_vec[k]=0;
        for(j=0;j<nparticles;j++) ml_vec[k]+=weights[j];
        ml_vec[k]/=nparticles;

    }

    LM=ml_vec[0]; // this is already a log!
    for(k=1;k<datasize;k++) {
        LM+=log(ml_vec[k]);
        if(LM!=LM) {
            std::cout<<"problems at k = "<<k<<std::endl;
            std::cout<<"log(ml_vec["<<k<<"] = "<<ml_vec[k]<<std::endl;
            abort();
        }
    }

    for(k=1;k<datasize;k++) {
        LM+=maxlw_vec[k];
        if(LM!=LM) {
            std::cout<<"problems!"<<std::endl;
            abort();
        }
    }

    resampling = gsl_ran_discrete_preproc(nparticles, &weights[0]);
    j=gsl_ran_discrete(rng[omp_get_thread_num()],resampling);
    gsl_ran_discrete_free(resampling);

    writeSampledParticle(j,X);
    LogMarginalLikelihood=LM;

}

// Implementation of particle filter test
void smc_exp::condSMC(const smc_exp_parameters & params,CalciumTrace &X)
{

    unsigned int i,j,k;

    std::vector<double> ml_vec(datasize);

    gsl_ran_discrete_t *resampling;
    std::vector<double> logw(nparticles);
    std::vector<double> maxlw_vec(datasize);

    for(i=0;i<nparticles;++i){
        
        if(i==nparticles-1){
            particles[i].calcium[0]=X.calcium[0];
            particles[i].baseline[0]=X.baseline[0];
            particles[i].spikes[0]=X.spikes[0];
        } else {
            particles[i].calcium[0]=params.C0;
            particles[i].baseline[0]=gsl_ran_gaussian(rng[omp_get_thread_num()],params.sdB0)+params.B0;
            particles[i].spikes[0]=0;
        }

        logw[i]=gaussian_pdf_log(particles[i].baseline[0]+params.C0-data[0],0,params.sdX);
    }

    k=0; while(k<datasize){

        wfrom_logw(logw,weights,maxlw_vec[k]);

        ml_vec[k]=0;

        #ifdef _OPENMP
        #pragma omp parallel for default(shared) private(j)
        #endif
        for(j=0;j<nparticles;j++) 
            ml_vec[k]+=weights[j];

        ml_vec[k]/=nparticles;

        k++;

        resampling = gsl_ran_discrete_preproc(nparticles, &weights[0]);

        #ifdef _OPENMP
        #pragma omp parallel for default(shared) private(j)
        #endif
        for(j=0;j<nparticles-1;++j) 
            parent[j][k]=gsl_ran_discrete(rng[omp_get_thread_num()],resampling);

        parent[nparticles-1][k]=nparticles-1;
        gsl_ran_discrete_free(resampling);

        #ifdef _OPENMP
        #pragma omp parallel for default(shared) private(j)
        #endif
        for(j=0;j<nparticles-1;++j){
            
            double logZS{0.};
            i=parent[j][k];

            // Draw s_t from P(s_t|x_{t-1},y_t)
            Rsbc_xtm1_yt(particles[j].spikes[k],particles[j].baseline[k],particles[j].calcium[k],logZS,
                            particles[i].spikes[k-1],particles[i].baseline[k-1],particles[i].calcium[k-1],
                            data[k], params);

            logw[j]=logZS;

        }

        // last particle
        j = nparticles-1; // should already be there but...
        i=parent[j][k];
        particles[j].spikes[k]=X.spikes[k];
        particles[j].baseline[k]=X.baseline[k];
        particles[j].calcium[k]=X.calcium[k];
        logw[j]=Dyt_xtm1(data[k],particles[i].spikes[k-1],particles[i].baseline[k-1],particles[i].calcium[k-1],params);

    }

    LogMarginalLikelihood=0;

    #ifdef _OPENMP
    #pragma omp parallel for default(shared) private(k) reduction(+:LogMarginalLikelihood)
    #endif
    for(k=0;k<datasize;k++) {
        LogMarginalLikelihood+=log(ml_vec[k]);
        if(LogMarginalLikelihood!=LogMarginalLikelihood) {
            std::cout<<"problems at k = "<<k<<std::endl;
            std::cout<<"log(ml_vec["<<k<<"] = "<<ml_vec[k]<<std::endl;
            abort();
        }
    }

    
    #ifdef _OPENMP
    #pragma omp parallel for default(shared) private(k) reduction(+:LogMarginalLikelihood)
    #endif
    for(k=0;k<datasize;k++) {
        LogMarginalLikelihood+=maxlw_vec[k];
        if(LogMarginalLikelihood!=LogMarginalLikelihood) {
            std::cout<<"problems!"<<std::endl;
            abort();
        }
    }

    std::ofstream output_weight("weights.dat");

    for(i=0;i<nparticles;++i){
        output_weight<<weights[i]<<std::endl;
    }

    output_weight.close();

    resampling = gsl_ran_discrete_preproc(nparticles, &weights[0]);
    j=gsl_ran_discrete(rng[omp_get_thread_num()],resampling);
    gsl_ran_discrete_free(resampling);

    writeSampledParticle(j,X);

}

void smc_exp::MCMC(std::vector<smc_exp_parameters> &parlist, std::vector<double> &d, size_t npart, size_t niter, std::ofstream &OUTFILE)
{
// open output files and delete their content
    std::ofstream output_cal("calcium.dat");
    std::ofstream output_sks("spikes.dat");
    std::ofstream output_b("baseline.dat");

    unsigned int i,j,k;
    double LML_old;
    double LM;
    PARTYPE variable;
    double alpha;
    double prior_new, prior_old;
    smc_exp_parameters pnew;
    parlist.resize(niter);
    parlist[0]=pnew;


    //Calculate data variance
    double data_mean=0;
    double data_var=0;
    for(k=0;k<d.size();++k) data_mean+=d[k]/d.size();
    for(k=0;k<d.size();++k) data_var+=pow(d[k]-data_mean,2)/d.size();
    for(k=0;k<d.size();++k) d[k]/=data_mean;

    OUTFILE<<"B0"<<' '
           <<"C0"<<' '
           <<"prob0"<<' '<<"prob1"<<' '
           <<"sdX"<<' '
           <<"gammaC"<<' '
           <<"sdB"<<' '
           <<"LML_old"<<std::endl;


    // This routine performs the MCMC over the parameters of the model.
    setData(&d[0],d.size());
    CalciumTrace X(datasize);
    InitParticles(npart);
    SMC(parlist[0],X,LM);
    LML_old=LogMarginalLikelihood;

    for(i=1;i<niter;++i){

        // update of parameters
        // choose variable to update
        variable = (PARTYPE)gsl_rng_uniform_int(rng[omp_get_thread_num()],6);
        //variable = PARTYPE::ALL;

        // Assign value to the old prior probability
        prior_old=pnew.prior(variable);
        // run proposal for that variable
        rproposal(pnew,parlist[i-1],variable);

        std::cout<<i<<std::endl;

        // recalculate LML
        SMC(pnew,X,LM);
        prior_new=pnew.prior(variable);

        alpha=(LogMarginalLikelihood-LML_old) +
                log(dproposal(parlist[i-1],pnew,variable)/dproposal(pnew,parlist[i-1],variable)) +
                prior_new-prior_old;

        if(gsl_rng_uniform(rng[omp_get_thread_num()])<exp(alpha)){
            parlist[i]=pnew;
            LML_old=LogMarginalLikelihood;
            prior_old=prior_new;
        } else {
            parlist[i]=parlist[i-1];
        }


        OUTFILE<<parlist[i].B0<<' '
               <<parlist[i].C0<<' '
               <<parlist[i].prob[0]<<' '<<parlist[i].prob[0]<<' '
               <<parlist[i].sdX<<' '
               <<parlist[i].gammaC<<' '
               <<parlist[i].sdB<<' '
               <<LML_old<<std::endl;

        for(k=0;k<datasize;++k){
            output_b<<X.baseline[k]<<' ';
            output_cal<<X.calcium[k]<<' ';
            output_sks<<X.spikes[k]<<' ';
        }

        output_b<<std::endl; output_cal<<std::endl; output_sks<<std::endl;

    }

    output_b.close();
    output_cal.close();
    output_sks.close();


}

void smc_exp::pGibbs(std::vector<smc_exp_parameters> &parlist, std::vector<double> &d, size_t npart, size_t niter, std::ofstream &OUTFILE)
{


    // open output files and delete their content
    std::ofstream output_cal("calcium.dat");
    std::ofstream output_sks("spikes.dat");
    std::ofstream output_b("baseline.dat");

    unsigned int i,k;
    smc_exp_parameters pnew ({});
    std::vector<double> d_tmp(d.size());
    parlist.resize(niter);
    parlist[0] = pnew;

    if(fromDATA){
        double data_mean=0;
        //    double data_var=0;
        for(k=0;k<d.size();++k) data_mean+=d[k]/d.size();
        //    for(k=0;k<d.size();++k) data_var+=pow(d[k]-data_mean,2)/d.size();
        for(k=0;k<d.size();++k) d[k]/=data_mean;
    }

    OUTFILE<<"B0"<<' '
           <<"C0"<<' '
           <<"prob0"<<' '<<"prob1"<<' '
           <<"sdX"<<' '
           <<"gammaC"<<' '
           <<"sdB"<<' '
           <<"LML"<<std::endl;


    // This routine performs the particle Gibbs sampling over the parameters of the model.
    setData(&d[0],d.size());

    CalciumTrace X(datasize);
    InitParticles(npart);
    runtest(d_tmp,parlist[0],X);

    std::cout <<std::endl << "Starting Particle Gibbs for "<< niter << " iterations." << std::endl;

    for(i=0;i<niter;++i){

        if( (i+1) % 100 == 0 )
           std::cout<<"\rIteration "<<i+1<<"             " << std::flush;//<<std::endl;

        rGibbsParams(parlist[i],X);
        condSMC(parlist[i],X);



        OUTFILE<<parlist[i].B0<<' '
               <<parlist[i].C0<<' '
               <<parlist[i].prob[0]<<' '<<parlist[i].prob[0]<<' '
               <<parlist[i].sdX<<' '
               <<parlist[i].gammaC<<' '
               <<parlist[i].sdB<<' '
               <<LogMarginalLikelihood<<std::endl;

        for(k=0;k<datasize;++k){
            output_b<<X.baseline[k]<<' ';
            output_cal<<X.calcium[k]<<' ';
            output_sks<<X.spikes[k]<<' ';
        }

        output_b<<std::endl; output_cal<<std::endl; output_sks<<std::endl;

    }

    std::cout <<std::endl << "Done! Writing output and exiting" << std::endl<< std::endl;

    output_b.close();
    output_cal.close();
    output_sks.close();


}

// M: I'll just ignore the below function for now, thus rng there is kinda buggy at the moment
void SMCsq(std::vector<double> &d, size_t theta_npart, size_t X_npart, std::ofstream &OUTFILE)
{
    unsigned int i,j,k;
    unsigned int Xi, Ti;
    double logZS;

    gsl_rng *rng;

    rng = gsl_rng_alloc (gsl_rng_mt19937);
    gsl_rng_set(rng,1);

    // theta-particles
    std::vector<smc_exp_parameters> theta_particles(theta_npart);
    std::vector<smc_exp_parameters> theta_particles_new(theta_npart);

    // Sequential montecarlo samplers assigned to each theta-particle
    std::vector<smc_exp*> X_smc(theta_npart);
    // Weights of theta-particles initialized to 1
    std::vector<double> theta_weights(theta_npart,1);

    for(Ti=0;Ti<theta_npart;Ti++){
        // Initialize theta-particles using the prior distribution
        theta_particles[Ti].rprior(rng);

        // Initialize SMCs
        X_smc[Ti] = new smc_exp({(int)Ti});
        X_smc[Ti]->setData(&d[0],d.size());
        X_smc[Ti]->InitParticles(X_npart);
    }

    // Define the ancestors for theta-particles.
    // Not keeping track of the entire lineage but only the current time point.
    std::vector<int> theta_ancestors(theta_npart);

    // Log-Marginal likelihood P(Y_t|Y_{1:t-1},\theta) for all theta-particles
    // NOTE: This can probably also be calculated only at current time.
    std::vector<std::vector<double> > X_ml_vec(theta_npart,std::vector<double>(d.size()));

    // resampling random generators for theta and X particles.
    gsl_ran_discrete_t *resampling;
    gsl_ran_discrete_t *theta_resampling;

    // theta-particle and X-particle log weights
    std::vector<double> theta_logw(theta_npart);
    std::vector<std::vector<double> > X_logw(theta_npart,std::vector<double>(X_npart));

    // theta-particle and X-particle max log weights
    std::vector<double> theta_maxlw_vec(d.size());
    std::vector<std::vector<double> > X_maxlw_vec(theta_npart,std::vector<double>(d.size()));

    // Initialize all X-particles for all theta-particles
    for(Ti=0;Ti<theta_npart;Ti++){
        for(Xi=0;Xi<X_npart;Xi++){
            X_smc[Ti]->particles[Xi].calcium[0]=theta_particles[Ti].C0;
            X_smc[Ti]->particles[Xi].baseline[0]=theta_particles[Ti].B0;
            X_smc[Ti]->particles[Xi].spikes[0]=0;
            X_smc[Ti]->weights[Xi]=1;
        }
    }

    // Propagate theta and X particles over time
    for(k=1;k<d.size();++k){

        theta_resampling = gsl_ran_discrete_preproc(theta_npart, &theta_weights[0]);
        for(Ti=0;Ti<theta_npart;Ti++){
            theta_ancestors[Ti]=gsl_ran_discrete(rng,theta_resampling);
            theta_particles_new[Ti]=theta_particles[theta_ancestors[Ti]];
        }

        for(Ti=0;Ti<theta_npart;Ti++){
            theta_particles[Ti]=theta_particles_new[Ti];
        }

        gsl_ran_discrete_free(theta_resampling);

        CalciumTrace CT(k);

        // propagate X particles for each theta particle
        for(Ti=0;Ti<theta_npart;Ti++){

            resampling = gsl_ran_discrete_preproc(X_npart, &(X_smc[Ti]->weights[0]));
            for(Xi=0;Xi<X_npart;++Xi) X_smc[Ti]->parent[Xi][k]=gsl_ran_discrete(rng,resampling);
            gsl_ran_discrete_free(resampling);

            for(Xi=0;Xi<X_npart;++Xi){
                i=X_smc[Ti]->parent[Xi][k];

                // Draw s_t from P(s_t|x_{t-1},y_t)
                X_smc[Ti]->Rsbc_xtm1_yt(X_smc[Ti]->particles[Xi].spikes[k],
                                        X_smc[Ti]->particles[Xi].baseline[k],
                                        X_smc[Ti]->particles[Xi].calcium[k],
                                        logZS,
                                        X_smc[Ti]->particles[i].spikes[k-1],
                        X_smc[Ti]->particles[i].baseline[k-1],
                        X_smc[Ti]->particles[i].calcium[k-1],
                        d[k], theta_particles[Ti]);
                X_logw[Ti][Xi]=logZS;

            }

            wfrom_logw(X_logw[Ti],X_smc[Ti]->weights,X_maxlw_vec[Ti][k]);

            X_ml_vec[Ti][k]=0;
            for(Xi=0;Xi<X_npart;Xi++) X_ml_vec[Ti][k]+=X_smc[Ti]->weights[Xi];
            X_ml_vec[Ti][k]/=X_npart;

            theta_logw[Ti]+=log(X_ml_vec[Ti][k]);
            theta_logw[Ti]+=X_maxlw_vec[Ti][k];

        }

        wfrom_logw(theta_logw,theta_weights,theta_maxlw_vec[k]);

        for(Ti=0;Ti<theta_npart;Ti++){

            resampling = gsl_ran_discrete_preproc(X_npart, &X_smc[Ti]->weights[0]);
            j=gsl_ran_discrete(rng,resampling);
            gsl_ran_discrete_free(resampling);
            X_smc[Ti]->writeSampledParticle(j,CT);
            X_smc[Ti]->rGibbsParams(theta_particles[Ti],CT);


        }

    }
}
