source('r/gaussian/particle_filter.R')

require(MCMCpack)
require(truncnorm)

ldinvgamma = function(x,a,b){
  return(
    a*log(b) - lgamma(a) + (-a-1)*log(x) -(b/x)
  )
}

getDefaultPriorPar = function()
  return(list(
    a_p = 1, b_p = 10,
    
    a_c = 0.5, b_c = 15,
    a_b = 0.5, b_b = 15,
    a_x = 0.5, b_x = 15,
    
    c0_mu = 0, c0_sd = 5,
    b0_mu = 0, b0_sd = 5
  ))

getDefaultProposalPar = function()
  return(list(
    p_q_mu = 0,          ## p is trunc norm
    p_q_sd = 1e-2,
    
    s2c_q_mu=0,            ## sigma_2s are proposed with a lognormal RW
    s2c_q_sd=0.05,
    s2b_q_mu=0,
    s2b_q_sd=0.02,
    s2x_q_mu=0,
    s2x_q_sd=0.02,
    
    b0_q_mu = 0,         ## b_0 and c_0 are normals centered in 0 with high variance
    b0_q_sd = 1,
    c0_q_mu = 0,
    c0_q_sd = 1
  ))


dlogPrior = function(par,prior_par=NULL)
{
  
  ## Prior list
  # p ~ Beta(a_p,b_p) with a_p=1, b_p=1  (i.e. uniform?)
  # sigma2_c ~ IG(a_c,b_c) with a_c = 1, b_c = 2
  # sigma2_b  ~ G(a_b,b_b) with a_b = 1, b_b = 2  ## this are supposed to be small!! (IG has always peak >0)
  # sigma2_x  ~ G(a_x,b_x) with a_x = 1, b_x = 2
  # c_0 ~ N(0,100)
  # b_0 ~ N(0,100)
  
  if( is.null(prior_par))
  {
    prior_par = getDefaultPriorPar()
  }
  
  attach(prior_par)
  
  lp = 0
  
  lp = lp + dbeta(par$p,a_p,b_p,log=TRUE)
  
  lp = lp + ldinvgamma(par$sigma2_c,a_c,b_c)
  lp = lp + dgamma(par$sigma2_b,a_b,b_b,log=TRUE)
  lp = lp + dgamma(par$sigma2_x,a_x,b_x,log=TRUE)
  
  lp = lp + dnorm(par$b_0,b0_mu,b0_sd,log=TRUE)
  lp = lp + dnorm(par$c_0,c0_mu,c0_sd,log=TRUE)
  
  detach(prior_par)
  return(lp)
  
}

rPrior = function(par,prior_par=NULL)
{
  
  if( is.null(prior_par))
  {
    prior_par = getDefaultPriorPar()
  }
  
  attach(prior_par)
  
  par_new = par
  
  par_new$p = rbeta(1,a_p,b_p)
  
  par_new$sigma2_c =  rinvgamma(1,a_c,b_c)
  par_new$sigma2_b = rgamma(1,a_b,b_b)
  par_new$sigma2_x = rgamma(1,a_x,b_x)
  
  par_new$b_0 = rnorm(1,b0_mu,b0_sd)
  par_new$c_0 = rnorm(1,c0_mu,c0_sd)
  
  detach(prior_par)
  
  
  return(par_new)  
}


dlogProposal = function(par_new,par,
                        proposal_par = NULL
)
{
  
  if( is.null(proposal_par) )
  {
    proposal_par = getDefaultProposalPar()
  }
  
  attach(proposal_par)
  
  lp = 0
  
  lp = lp + log(dtruncnorm(par_new$p,0,1,p_q_mu+par$p,p_q_sd))
  
  lp = lp + dnorm(log(par_new$sigma2_c),s2c_q_mu+log(par$sigma2_c),s2c_q_sd,log=TRUE)
  lp = lp + dnorm(log(par_new$sigma2_b),s2b_q_mu+log(par$sigma2_b),s2b_q_sd,log=TRUE)
  lp = lp + dnorm(log(par_new$sigma2_x),s2x_q_mu+log(par$sigma2_x),s2x_q_sd,log=TRUE)
  
  lp = lp + dnorm(par_new$c_0,b0_q_mu+par$b_0,b0_q_sd,log=TRUE)
  lp = lp + dnorm(par_new$c_0,c0_q_mu+par$c_0,c0_q_sd,log=TRUE)
  
  detach(proposal_par)
  return(lp)
  
}

rProposal = function(par, 
                     update_idx = c(1,2,3,4,5,6), ## only indexes in this array get update
                     proposal_par = NULL )
{
  
  if( is.null(proposal_par) )
  {
    proposal_par = getDefaultProposalPar()
  }
  
  attach(proposal_par)
  
  par_new = par
  
  if(1%in%update_idx) par_new$p = rtruncnorm(1,0,1,p_q_mu+par$p,p_q_sd)
  
  if(2%in%update_idx) par_new$sigma2_c = exp(rnorm(1,s2c_q_mu+log(par$sigma2_c),s2c_q_sd))
  if(3%in%update_idx) par_new$sigma2_b = exp(rnorm(1,s2b_q_mu+log(par$sigma2_b),s2b_q_sd))
  if(4%in%update_idx) par_new$sigma2_x = exp(rnorm(1,s2x_q_mu+log(par$sigma2_x),s2x_q_sd))
  
  if(5%in%update_idx) par_new$b_0 = rnorm(1,b0_q_mu+par$b_0,b0_q_sd)
  if(6%in%update_idx) par_new$c_0 = rnorm(1,c0_q_mu+par$c_0,c0_q_sd)
  
  detach(proposal_par)
  return(par_new)  
}


pMCMC = function( Fluo_trj , n_iter = 10000, 
                  model_init = NULL,
                  
                  ## Prior parameters
                  prior_par = NULL,
                  
                  ## MCMC proposal parameter
                  proposal_par = NULL,
                  
                  ## particle Filter proposal variances
                  pf_par = NULL,
                  
                  ## Number of particles in the pf estimate of the ML
                  n_particles = 20)
{
  ## Parameters list
  # p = probability of a spike s_t ~ Bern(p)   --  TODO make this Markov as well
  # sigma2_c = variance of the calcium process (given s_t = 1)
  # sigma2_b = variance of the baseline random walk process
  # sigma2_x =variance of the fluorescence noise
  # c_0 = starting calcuim
  # b_0 = starting baseline
  
  ## Prior list
  # p ~ Beta(a_p,b_p) with a_p=1, b_p=1  (i.e. uniform?)
  # sigma2_c ~ IG(a_c,b_c) with a_c = 1, b_c = 2
  # sigma2_b  ~ IG(a_b,b_b) with a_b = 1, b_b = 2
  # sigma2_x  ~ IG(a_x,b_x) with a_x = 1, b_x = 2
  # c_0 ~ N(0,100)
  # b_0 ~ N(0,100)
  
  ## Parameters initialisation
  # p = 0.01
  # sigma2_c = var(Fluo_trj)/3
  # sigma2_b = var(Fluo_trj)/3
  # sigma2_x = var(Fluo_trj)/3
  # b_0 = mean(Fluo_trj)
  # c_0 = Fluo_trj_1 - b_0
  
  if("model_init" %in% search() )
    detach(model_init)
  if("pf_par" %in% search() )
    detach(pf_par)
  if("prior_par" %in% search() )
    detach(prior_par)
  if("proposal_par" %in% search() )
    detach(proposal_par)
  
  if( is.null(model_init) )
  {
    model_init = list(
      
      T = length(Fluo_trj),
      lambda = 0.03,
      
      p = 0.01,
      sigma2_c = var(Fluo_trj)/2,
      sigma2_b = 1e-2, #var(Fluo_trj)/3,
      sigma2_x = 1e-2  #var(Fluo_trj)/3
    )
    
    model_init$b_0 = Fluo_trj[1] + rnorm(1,0,model_init$sigma2_x) # mean(Fluo_trj[1:5]))
    model_init$c_0 = 0 # Fluo_trj[1] - model_init$b_0
  }
  
  if( is.null(prior_par))
  {
    prior_par = getDefaultPriorPar()
  }
  
  if( is.null(proposal_par) )
  {
    proposal_par = getDefaultProposalPar()  
  }
  
  if( is.null( pf_par ))
  {
    pf_par = getDefaultPFPar()
  }
  
  p_init = model_init$p
  s2c_init = model_init$sigma2_c
  s2b_init = model_init$sigma2_b
  s2x_init = model_init$sigma2_x
  b0_init = model_init$b_0
  c0_init = model_init$c_0
  
  ## Chain init
  mcmc_p = array(NA,c(n_iter)); mcmc_p[1] = p_init
  mcmc_s2c = array(NA,c(n_iter)); mcmc_s2c[1] = s2c_init
  mcmc_s2b = array(NA,c(n_iter)); mcmc_s2b[1] = s2b_init
  mcmc_s2x = array(NA,c(n_iter)); mcmc_s2x[1] = s2x_init
  mcmc_b0 = array(NA,c(n_iter)); mcmc_b0[1] = b0_init
  mcmc_c0 = array(NA,c(n_iter)); mcmc_c0[1] = c0_init
  
  par_curr = model_init
  log_ml_curr = particleFilter(N = n_particles, pf_par = pf_par,
                               Fluo_trj = Fluo_trj, model_parameter = par_curr,
                               quiet = TRUE)$logML
  log_prior_curr = dlogPrior(par_curr,prior_par)
  
  mcmc_lML = array(NA,c(n_iter)); mcmc_lML[1] = log_ml_curr
  
  n_update_par = 2 ## number of parameteres to update at once
  
  cat('\n')
  acc = vector("numeric",100); acc[1]=1
  tick = 10
  stuck = 0
  
  for(iter in 2:n_iter)
  {
    ## decide which param to update
    update_index = sample(1:4,n_update_par,replace=TRUE)  
    ## n_update_par at time (replace means that at times less than n_update might be updated, it's ok)
    
    ## propose new parameters
    par_prop = rProposal(par_curr,update_index,proposal_par)
    
    ## compute new MLvalue and prior
    log_ml_prop = particleFilter(N = n_particles, pf_par = pf_par,
                                 Fluo_trj = Fluo_trj, model_parameter = par_prop,
                                 quiet = TRUE)$logML
    log_prior_prop = dlogPrior(par_prop,prior_par)
    
    ## compute acceptance prob
    logA = (log_ml_prop - log_ml_curr) + (log_prior_prop - log_prior_curr) + 
      ( dlogProposal(par_curr,par_prop,proposal_par) - dlogProposal(par_prop,par_curr,proposal_par) )
    
    ## MH Step
    if( log(runif(1)) < logA )
    {
      ## accept
      par_curr = par_prop
      log_ml_curr = log_ml_prop
      log_prior_curr = log_prior_prop
      
      acc[ iter %% 100 ] = 1
    }else{
      acc[ iter %% 100 ] = 0 }
    
    ## update mcmc status
    mcmc_p[iter] = par_curr$p
    mcmc_s2c[iter] = par_curr$sigma2_c
    mcmc_s2b[iter] = par_curr$sigma2_b
    mcmc_s2x[iter] = par_curr$sigma2_x
    mcmc_b0[iter] = par_curr$b_0
    mcmc_c0[iter] = par_curr$c_0
    
    ## Update proposal
    # if(iter == 1000){
    if( FALSE){  
      proposal_par$s2c_q_sd = 0.8 * sd(log(mcmc_s2c[1:iter])) + 0.2 * proposal_par$s2c_q_sd
      proposal_par$s2b_q_sd = 0.8 * sd(log(mcmc_s2b[1:iter])) + 0.2 * proposal_par$s2b_q_sd
      proposal_par$s2x_q_sd = 0.8 * sd(log(mcmc_s2x[1:iter])) + 0.2 * proposal_par$s2x_q_sd

      proposal_par$p_q_sd = 0.8 * sd(mcmc_p[1:iter]) + 0.2 * proposal_par$p_q_sd

      proposal_par$b0_q_sd = 0.8 * sd(mcmc_b0[1:iter]) + 0.2 * proposal_par$b0_q_sd
      proposal_par$c0_q_sd = 0.8 * sd(mcmc_c0[1:iter]) + 0.2 * proposal_par$c0_q_sd
      
    }
    
    # if( FALSE )
    if( iter > 1000 && (iter %% tick ) == 0 )
    {
      if( mean(acc) < 0.2 ){
        
        const = 0.9
        
        proposal_par$s2c_q_sd = proposal_par$s2c_q_sd * const
        proposal_par$s2b_q_sd = proposal_par$s2b_q_sd * const
        proposal_par$s2x_q_sd = proposal_par$s2x_q_sd * const
        
        proposal_par$p_q_sd = proposal_par$p_q_sd * const
        
        proposal_par$b0_q_sd = proposal_par$b0_q_sd * const
        proposal_par$c0_q_sd = proposal_par$c0_q_sd * const
        
      }else if( mean(acc) > 0.4 ){
        const = 1.15
        
        proposal_par$s2c_q_sd = proposal_par$s2c_q_sd * const
        proposal_par$s2b_q_sd = proposal_par$s2b_q_sd * const
        proposal_par$s2x_q_sd = proposal_par$s2x_q_sd * const
        
        proposal_par$p_q_sd = proposal_par$p_q_sd * const
        
        proposal_par$b0_q_sd = proposal_par$b0_q_sd * const
        proposal_par$c0_q_sd = proposal_par$c0_q_sd * const
        
      }
      
    }
    # sanity check
    if( mean(acc) == 0 ){
      stuck = stuck+1
    }
    
    if( stuck >= 10 ){
      stuck=0
      # regenerate ML
      cat("\r REGENNING  -> ",log_ml_curr)
      log_ml_curr = particleFilter(N = n_particles, pf_par = pf_par,
                                   Fluo_trj = Fluo_trj, model_parameter = par_curr,
                                   quiet = TRUE)$logML
      cat("  -> ",log_ml_curr,"\n")
    }
    
    ## Updte curr_lML
    mcmc_lML[iter] = log_ml_curr
    
    ## Say something
    cat("\r                                                                       ",
        "\r pMCMC Iteration ",iter," / ",n_iter, "  - ar = ",mean(acc))
  }
  cat('\n')
  
  
  return( list(
    trace = list(
      p= mcmc_p,
      s2c = mcmc_s2c,
      s2b = mcmc_s2b,
      s2x = mcmc_s2x,
      b0 = mcmc_b0,
      c0 = mcmc_c0,
      lML = mcmc_lML
    ), prior_par = prior_par , proposal_par = proposal_par , pf_par = pf_par
  ))
}




