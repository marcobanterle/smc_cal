# pMCMC sampler for calcium traces

WIP

## Usage

`make`
`./bin/main [--useData] [--nIter X] [--nXParticles X] [--dataLength X]`

* `--useData` : If given, a real data file will be used, otherwise a simulated trajectory will be sampled;
* `--dataLength` : `X` specifies either the length of the synthetic trajectory to sample or the number of datapoint to extract from the real-data trace;
* `--nIter` : `X` specifies the number of iteration for the pMCMC chain;
* `--nXParticles` : `X` specifies the number of particles in the (conditional) Particle Filters for each iteration of the chain;
  
## Input 

* `init_parameters.dat` : if the sampler is called WITHOUT `--useData` and will thus simulate a trace, it will first search in here for the parameters to use;
  
    In order it expects $B_0$, $\sigma_b$, $\gamma_c$, $\sigma_x$, $p_0$ and $p_1$ ;

    Note that if the file is not complete it will `throw` an error, so if in doubt erase it completely.
    
* `...` if `--useData` is specified, main.cxx knows where to look for a trajectory file.

## Output

* `sampler_parameters.dat` : contains some of the arguments used to call the sampler( `nIter`, `nXParticles` and `dataLength` );
* `out.dat` : contains the $y$ trajectory used; if run on simulated data other columns of, respectively, $s$, $b$ and $c$ will be there;
* `chain.dat` : contains the parameters sampled from the pMCMC chain, as well as the log marginal likelihood,for each iteration;

### For some algorithms
* `calcium.dat`, `baselie.dat` and `spikes.dat` will contain a sampled trajectories from the PF, on the rows, for each iteration of the pMCMC.
