
#include<gsl/gsl_rng.h>
#include <gsl/gsl_randist.h>
#include <vector>
#include "calciumtrace.h"

class smc_parameters{
public:
    smc_parameters();
    double prior(double z,double gv);
    double sdX;
    double sdB;
    double sdC;
    double lambda;
    double B0;
    double C0;
    double prob;
    double freq;
};

class smc_traj{

public:
    smc_traj(int seed);
    void InitParticles(size_t);
    void SMC(smc_parameters &);
    void MCMC(std::vector<smc_parameters>& chain,std::vector<double> & data, size_t nparticles,size_t chain_size, std::ofstream &);
    void MaxML(std::vector<smc_parameters>& chain,std::vector<double> & data, size_t nparticles,size_t chain_size, std::ofstream &);
    void setData(double*,int n);
    void writeParticles();
    double dproposal(smc_parameters & pnew,smc_parameters &pold);
    void rproposal(smc_parameters & pgen,smc_parameters &pold,int var);
    void runtest(std::vector<double> &, smc_parameters &);
    double MarginalLikelihood;

private:
    size_t nparticles;
    int rngseed;
    gsl_rng *rng;
    double* data;
    int datasize;
    double sigmaX_prop, sigmaB_prop, sigmaC_prop;
    double beta_N, sdC0_prop_mcmc,sdB0_prop_mcmc,gamma_sd;
    std::vector<CalciumTrace> particles;
    std::vector<std::vector<int> > parent;
    std::vector<double> weights;

};


