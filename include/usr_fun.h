#ifndef USR_FUN_H
#define USR_FUN_H

#include <vector>

double gaussian_pdf_log(double x,double mean,double sd);
void wfrom_logw(std::vector<double> &lw,std::vector<double> &w,double& maxlw);


#endif // USR_FUN_H