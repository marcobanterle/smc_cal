#ifndef CALCIUMTRACE_H
#define CALCIUMTRACE_H

#include <vector>

enum class PARTYPE {
    B0,
    C0,
    PROB0,
    SDB,
    GAMMAC,
    SDX,
    PROB1,
    ALL
};

class CalciumTrace
{
public:
    CalciumTrace(int);
    int size;
    std::vector<double> calcium;
    std::vector<double> baseline;
    std::vector<int> spikes;
};

#endif // CALCIUMTRACE_H
