
#include<gsl/gsl_rng.h>
#include <gsl/gsl_randist.h>
#include <vector>
#include "../include/calciumtrace.h"

class smc_exp_parameters{
public:
    smc_exp_parameters();
    // ~smc_exp_parameters() { std::cout << " Bye! " <<std::flush; }
    smc_exp_parameters(const smc_exp_parameters &par);

    double prior(PARTYPE);
    void rprior(const gsl_rng* rng);
    void print();
    double sdX;
    int tdist_nu;
    double sdB;
    double gammaC;
    double lambda;
    double B0; //this is the mean of the distribution of B at time 0
    double sdB0; // and the corresponding variance
    double C0;
    std::vector<double> prob;
    double freq;
    double dt;

    smc_exp_parameters operator= (const smc_exp_parameters&);
};

class smc_exp{

public:
    smc_exp(std::vector<long long> seed);

    void InitParticles(size_t);
    void SMC(smc_exp_parameters &, CalciumTrace&,double &LM);
    void condSMC(const smc_exp_parameters & params,CalciumTrace &X);    
    void MCMC(std::vector<smc_exp_parameters>& chain,std::vector<double> & data, size_t nparticles,size_t chain_size, std::ofstream &);
    void pGibbs(std::vector<smc_exp_parameters> &parlist, std::vector<double> &d, size_t npart, size_t niter, std::ofstream &OUTFILE);

    friend void SMCsq(std::vector<double> &d, size_t theta_npart, size_t X_npart, std::ofstream &OUTFILE);

    void setData(double*,int n);
    void writeParticles();
    void writeSampledParticle(unsigned int, CalciumTrace&);
    double dproposal(smc_exp_parameters & pnew,smc_exp_parameters &pold,PARTYPE);
    void rproposal(smc_exp_parameters & pgen,smc_exp_parameters &pold,PARTYPE);
    void rGibbsParams(smc_exp_parameters& p, const CalciumTrace &X);
    void runtest(std::vector<double> &, smc_exp_parameters &, CalciumTrace& ct);
    void Rsbc_xtm1_yt(int &s1, double &b1, double &c1, double &ZS,
                 int s0,double b0,double c0,
                 double y1, const smc_exp_parameters & );
    double Dyt_xtm1(double y1,int s0, double b0, double c0 ,const smc_exp_parameters & );
    double MarginalLikelihood;
    double LogMarginalLikelihood;
    bool fromDATA;

private:
    size_t nparticles;
    std::vector<long long> rngseed;
    std::vector<gsl_rng*> rng;
    double* data;
    int datasize;
    double alphaX,betaX,alphaC, betaC,alphaB,betaB,alphaq,betaq;
    double prior_sdB0, alphaB0, betaB0;
    double sigmaX_prop, sigmaB_prop, shapeC_prop;
    double beta_N, gammaC0_prop_mcmc,sdB0_prop_mcmc,gamma_shape;
    std::vector<CalciumTrace> particles;
    std::vector<std::vector<int> > parent;
    std::vector<double> weights;   

};


